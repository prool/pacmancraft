char *help_text = "Command list:\n\
blind - режим слепого игрока (blind mode switcher)\n\
koi, win, utf, lat - switch codetable\n\
codetable - print current codetable\n\
alf - print Russian alfavit\n\
ascii - print ascii table\n\
normcolor, nocolor, contrast - color modes\n\
sysinfo - print sysinfo\n\
source - print source\n\
ls - ls\n\
dir-up, dir-down, dir-move - смена выделенного файла\n\
cd - change directory\n\
cd.. or .. - cd ..\n\
cd/ - cd /\n\
pwd - print work directory\n\
delfile - delete file\n\
createfile - create new file from stdin\n\
cat - вывод выделенного файла на экран\n\
hcat - hex вывод выделенного файла на экран\n\
stat - вывод статистики о выделенном файле\n\
sh - shell\n\
\n\
blog - Prool's blog\n\
testesc - test of ESC codes\n\
test2 - test of keyboard\n\
test3 - test of UTF-8\n\
test4 - test of cyrillic in UTF-8\n\
testcolor256 - test of ANSI 256 colors\n\
testrgb - test of ANSI RGB colors\n\
cls - clearscreen\n\
reset - reset terminal\n\
look, см, . - look\n\
score, сч, счет - счет (очки, запасы, прочее)\n\
шире, уже, выше, ниже - смена размеров показываемого поля\n\
\n\
directions: 1 step: n, s, w, e (от слов north, south, etc) or arrow+enter\n\
N steps: digit and direction, f.e.  10n\n\
maximum steps: N, S, W, E\n\
gohome - go home\n\
\n\
inv - print inventory\n\
get - get object\n\
drop - drop object\n\
swap - swap objects in inventory and in room\n\
dup - duplicate object\n\
create - create object\n\
destroy - destroy object\n\
random - random symbols to screen\n\
langton - run Langton ant\n\
\n\
room edit commands: resetroom, roomcolor, roombg, roomsymbol, roomsymbolcode, roomtype, roomdescr\n\
save - save world\n\
olist - objects list\n\
\n\
env - print environment\n\
date - print date & time\n\
holyday - print holyday\n\
gpl3 - print GNU License ver.3\n\
rog - rogalik (use arrows; q, Q - quit)\n\
rt - realtime rogalik (q, Q - quit)\n\
rt0 - old realtime (q, Q - quit)\n\
skript - run proolskript interpreter\n\
till, пахать - пахать\n\
sow, сеять - сеять\n\
harvest, убирать - убирать урожай\n\
dig - копать\n\
q, quit, exit, конец - exit\n\
\n\
Sites: prool.kharkov.org gitlab.com/prool/pacmancraft\n\
Prool's email proolix dog gmail.com\n\
Prool's phone +380 (066) 297-7515\n";
