#makefile for MacOS
pacmancraft: pacmancraft.c pacmancraft.h world.h proolskript.c readw.c aux-skripts.c roomtypes.h help.c
	gcc pacmancraft.c -o pacmancraft -DFREEBSD -DMACOS -I /usr/local/include /usr/lib/libiconv.dylib
	strip pacmancraft
clean:
	rm pacmancraft
